import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private email: string|undefined;

  constructor(private httpClient: HttpClient) { }

  setEmail(email: string) {
    this.email = email;
  }

  getEmail() {
    return this.email;
  }

  loginUser(username: string, password: string) {
    return this.httpClient.post('https://symfony-auth.aaa/api/login', {
      username: username,
      password: password,
    }).subscribe((data) => {
      // @ts-ignore
      this.setEmail(data.email);
    });
  }
}
