import {Component, Injectable} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthService} from "./auth.service";

@Injectable({providedIn: 'root'})
@Component({
  selector: '[app-root]',
  standalone: true,
  imports: [RouterOutlet, FormsModule, ReactiveFormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  username = new FormControl('');
  password = new FormControl('');

  constructor(public authService: AuthService) {
  }

  loginUser(event: SubmitEvent) {
    event.preventDefault();
    if (null === this.username.value || null === this.password.value) return;
    this.authService.loginUser(this.username.value, this.password.value);
  }
}
