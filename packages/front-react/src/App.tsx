import {FormEvent} from "react";
import ky from "ky";
import {createStore} from "zustand";
import React from 'react';

export const authStore = createStore<{uuid: string|undefined, setUuid: (uuid: string) => void}>((set) => ({
    uuid: undefined,
    setUuid: (uuid: string) => set({uuid}),
}))

function App() {
    const loginUser = React.useCallback(async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.currentTarget;
        const formData = new FormData(form)
        try {
            const response = await ky('https://symfony-auth.aaa/api/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: formData,
                credentials: 'include',
            })

            if (response.ok) {
                const data = await response.json() as { uuid: string };
                authStore.getState().setUuid(data.uuid);
            }
        } catch (error) {
        }
    }, []);

    return (
        <form method='POST' onSubmit={loginUser}>
      <h1>Connexion</h1>
        <div>
            <label htmlFor='email'>Email</label>
            <input type='email' id='email' name='email' required />
        </div>
        <div>
            <label htmlFor='password'>Mot de passe</label>
            <input type='password' id='password' name='password' required />
        </div>
        <button type="submit">Connexion</button>
    </form>
    )
}

export default App
