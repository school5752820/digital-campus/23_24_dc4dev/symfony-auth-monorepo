import {reactive} from "vue";

export const store = reactive<{email: string|undefined}>({
    email: undefined,
})